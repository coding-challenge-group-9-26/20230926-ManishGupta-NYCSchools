//
//  SchoolListView.swift
//  CitySchools
//
//  Created by Manish Gupta on 9/21/23.
//

import SwiftUI
import MapKit

extension CLLocationCoordinate2D {
    static let parking = CLLocationCoordinate2D(latitude: 42.354528, longitude: -71.068369)
}

struct SchoolListView: View {
    @ObservedObject var vm: SchoolViewModel = SchoolViewModel(url: Constants.nycSchoolsList)
//    @State var coordinateRegion: MKCoordinateRegion
    var body: some View {
        NavigationView {
            ScrollView {
                VStack(spacing: 10) {
                    ForEach(vm.isSearching ? vm.filteredSchools : vm.schools, id: \.self) { school in
                        NavigationLink {
                            SchoolDetails(viewModel: DetailsViewModel(school: school))
                        } label: {
                            VStack(alignment: .leading, spacing: 10) {
                                HStack(alignment: .top) {
                                    Text(school.school_name )
                                        .font(.headline)
                                    Spacer()
                                    Text(school.borough )
                                        .font(.caption)
                                }
                                
                                AddressView(school: school)
                                Text(school.admissionspriority11)
                                    .font(.caption)
                                Text(school.website)
                                    .underline()
                                    .onTapGesture {
                                        school.tappedOnWebsite(url: school.website)
                                    }
                                Text("📞 - \(school.phone_number)")
                                    .onTapGesture {
                                        school.tappedOnPhone(number: school.phone_number)
                                    }
                                
                                
                            }
                            .padding()
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .background(Color.black.opacity(0.05))
                            
                        }.buttonStyle(.plain)
                    }
                }.padding()
            }
            .navigationTitle("Schools in NYC")
            .navigationBarTitleDisplayMode(.inline)
            .searchable(text: $vm.searchText, placement: .automatic, prompt: "Search with School Names, Cities, Boroughs")
        }
        
    }
}

struct SchoolListView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolListView()
    }
}

struct AddressView: View {
    @State var school: School
    var body: some View {
        HStack {
            Text(school.primary_address_line_1)
                .font(.caption)
            Text(school.state_code)
                .font(.caption)
            Text(school.zip)
                .font(.caption)
        }
    }
}
