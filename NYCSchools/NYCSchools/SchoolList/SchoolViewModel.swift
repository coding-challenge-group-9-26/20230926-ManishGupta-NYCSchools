//
//  SchoolViewModel.swift
//  CitySchools
//
//  Created by Manish Gupta on 9/21/23.
//

import Foundation
import Combine

final class SchoolViewModel: ObservableObject {
    private var url: String
    private var error: Error?
    private var cancellables = Set<AnyCancellable>()
    var isSearching: Bool {
        !searchText.isEmpty
    }
    
    @Published var schools: [School] = []
    @Published var searchText: String = ""
    @Published var filteredSchools: [School] = []
    
    
    init(url: String) {
        self.url = url
        
        fetchSchools()
        addSubscribers()
    }
    
    private func fetchSchools() {
        listOfSchools { schools, error in
            DispatchQueue.main.async {
                if let schools = schools {
                    self.schools = schools
                }
            }
        }
    }
    
    private func listOfSchools(completion: @escaping([School]?, Error?) -> Void) {
        _ = Networking(with: self.url, type: School.self) { schools, error in
            if let schools = schools {
                completion(schools, nil)
            } else {
                completion(nil, error)
            }
        }
    }
    
    private func addSubscribers() {
        $searchText
            .debounce(for: 0.3, scheduler: DispatchQueue.main)
            .sink { [weak self] searchText in
                self?.filterSchools(searchText: searchText)
            }.store(in: &cancellables)
    }
    
    private func filterSchools(searchText: String) {
        guard !searchText.isEmpty else {
            filteredSchools = []
            return
        }
        
        let search = searchText.lowercased()
        filteredSchools = schools.filter({ school in
            let title = school.school_name.lowercased().contains(search)
            let city = school.city.lowercased().contains(search)
            let phone = school.phone_number.lowercased().contains(search)
            let borough = school.borough.lowercased().contains(search)
            
            return (title) || (city) || (borough) || phone
        })
    }
}
