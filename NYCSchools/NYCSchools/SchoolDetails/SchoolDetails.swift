//
//  SchoolDetails.swift
//  CitySchools
//
//  Created by Manish Gupta on 9/22/23.
//

import SwiftUI

struct SchoolDetails: View {
    var viewModel: DetailsViewModel
    var body: some View {
        VStack {
            HStack {
                Text(viewModel.school.school_name )
                    .multilineTextAlignment(.center)
                    .font(.title)
                    .padding(20)
            }
            ScrollView {
                Text(viewModel.school.overview_paragraph )
                    .padding(10)
            }
            
            List {
                HStack {
                    Text("Number of SAT Test Takers: ")
                    Text(viewModel.satScore.num_of_sat_test_takers ).italic()
                }
                HStack {
                    Text("Math Avg. Score: ")
                    Text(viewModel.satScore.sat_math_avg_score).italic()
                }
                HStack {
                    Text("Critical Reading Avg. Score: ")
                    Text(viewModel.satScore.sat_critical_reading_avg_score).italic()
                }
                HStack {
                    Text("Writing Avg. Score: ")
                    Text(viewModel.satScore.sat_writing_avg_score).italic()
                }
            }
        }
    }
}

struct SchoolDetails_Previews: PreviewProvider {
    static var previews: some View {
        SchoolDetails(viewModel: DetailsViewModel(school: School()))
    }
}
