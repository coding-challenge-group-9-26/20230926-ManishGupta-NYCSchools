//
//  DetailsViewModel.swift
//  CitySchools
//
//  Created by Manish Gupta on 9/22/23.
//

import Foundation

class DetailsViewModel: ObservableObject {
    var school: School
    @Published var satScore: SATScore = SATScore()
    
    init(school: School) {
        self.school = school
        fetchSatScore(dbn: school.dbn)
    }
    
    func fetchSatScore(dbn: String) {
        let url = Constants.scoreDetails + "dbn=\(dbn)"
        
        _ = Networking(details: url, type: SATScore.self) { score, error in
            if score?.count ?? 0 > 0, let score = score?.first {
                self.satScore = score
            }
        }
    }
}
