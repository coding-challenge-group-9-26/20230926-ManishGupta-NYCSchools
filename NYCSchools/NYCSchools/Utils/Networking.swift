//
//  Networking.swift
//  CitySchools
//
//  Created by Manish Gupta on 9/21/23.
//

import Foundation

class Networking {
    private var fileName: String = ""
    
    /// Initializer
    init<T: Decodable>(with url: String, type: T.Type,
                       completion: @escaping (([T]?, Error?) -> Void)) {
        
        if let dataExist = readCache() {
            do {
                try completion(JSONDecoder().decode([T].self, from: dataExist), nil)
            } catch {
                completion (nil, error)
            }
        } else {
            if let theURL = URL(string: url) {
                let request = URLRequest(url: theURL)
                
                URLSession.shared.dataTask(with: request) { data, _, error in
                    if let error = error {
                        completion(nil, error)
                    }
                    if let data = data {
                        do {
                            // Save to disk
                            self.cache(data: data)
                            let list = try JSONDecoder().decode([T].self, from: data)
                            completion(list, nil)
                        } catch {
                            completion(nil, error)
                        }
                    }
                }.resume()
            } else {
                let error: Error = NSError(domain: "com.def.abc.NoURLFound", code: 1)
                completion(nil, error)
            }
        }
    }
    
    init<T: Decodable>(details url: String, type: T.Type,
                       completion: @escaping (([T]?, Error?) -> Void)) {
        
        if let theURL = URL(string: url) {
            let request = URLRequest(url: theURL)
            
            URLSession.shared.dataTask(with: request) { data, _, error in
                if let error = error {
                    completion(nil, error)
                }
                if let data = data {
                    do {
                        let details = try JSONDecoder().decode([T].self, from: data)
                        completion(details, nil)
                    } catch {
                        completion(nil, error)
                    }
                }
            }.resume()
        } else {
            let error: Error = NSError(domain: "com.def.abc.NoURLFound", code: 1)
            completion(nil, error)
        }
    }

    
    /// Cache Data
    /// - Parameter data: Data to be persisted in a file
    private func cache(data: Data) {
        let documentDirectory = FileManager.default.urls(for: .documentDirectory,
                                                         in: .userDomainMask).first
        let fileName = Int(Date().timeIntervalSince1970)
        UserDefaults.standard.set(fileName, forKey: "fileName")
        if let pathWithFileName = documentDirectory?.appendingPathComponent("\(fileName)") {
            do {
                try data.write(to: pathWithFileName)
            } catch {
                print("Unable to write data")
            }
        }
    }
    
    /// Read from the cached data
    /// - Returns: Data type object
    private func readCache() -> Data? {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory,
                                                                .userDomainMask, true).first as? NSString
        var jsonString: String = ""
        let fileName = UserDefaults.standard.object(forKey: "fileName") as? Int
        
        guard let date = fileName else {
            return nil
        }
    
        if (Int(Date().timeIntervalSince1970) - date) > Constants.CACHE_DURATION {
            return nil
        } else {
            if let path = documentsPath?.appendingPathComponent("\(fileName ?? Int(Date().timeIntervalSince1970))") {
                do {
                    try jsonString = NSString(contentsOfFile: path,
                                            encoding: NSUTF8StringEncoding) as String
                }
                catch let error as NSError {
                    print("Error in reading from this file \(error.localizedDescription)")
                    return nil
                }
            }
        }
        
        return jsonString.data(using: .utf8)
    }
}
