//
//  Constants.swift
//  CitySchools
//
//  Created by Manish Gupta on 9/21/23.
//

import Foundation

enum Constants {
    static let nycSchoolsList = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    static let CACHE_DURATION = 24*60*60
    static var scoreDetails = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?"
    
}
