# 20230926 ManishGupta NYCSchools

## Getting started
1. Shows the list of Schools in the boroughs of NYC.
2. You can search using city, borough, school name or phone number.
3. The results are cached for 1 day (NYC List of schools)

Have used: 
1. MVVM Pattern
2. Swift with SwiftUI
3. Combine for search
